import request from '@/utils/request'

export function search(params) {
  return request({
    url: '/brand/search',
    method: 'post',
    data: params
  })
}

export function create(params) {
  return request({
    url: '/brand',
    method: 'post',
    data: params
  })
}

export function detail(id) {
  return request({
    url: '/brand/' + id,
    method: 'get'
  })
}

export function update(params) {
  return request({
    url: '/brand',
    method: 'put',
    data: params
  })
}

export function del(id) {
  return request({
    url: '/brand/' + id,
    method: 'delete'
  })
}

