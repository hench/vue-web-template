export default {
  route: {
    dashboard: '首页',
    material: '物料管理',
    brand: '物料品牌',
    category: '物料类别',
    materialList: '物料列表',
    example: '示例',
    table: '表格',
    tree: '树',
    form: '表单',
    nested: '嵌套菜单',
    menu1: '菜单1',
    'menu1-1': '菜单 1-1',
    'menu1-2': '菜单 1-2',
    'menu1-2-1': '菜单 1-2-1',
    'menu1-2-2': '菜单 1-2-2',
    'menu1-3': '菜单 1-3',
    'external Link': '外链'
  },
  login: {
    title: 'APL-WEB',
    userName: '请输入用户名',
    password: '请输入密码',
    userNameValidate: '请输入正确的用户名',
    passwordValidate: '请输入正确的密码',
    login: '登录'
  },
  common: {
    rowIndex: '序',
    name: '名称',
    add: '添加',
    edit: '修改',
    delete: '删除',
    export: '导出',
    detail: '详情',
    keyword: '关键字',
    ok: '确定',
    cancel: '取消'
  },
  material: {
    code: '代号',
    name: '名称',
    codeRequired: '请输入正确的代号',
    nameRequired: '请输入正确的名称',
    brandEdit: '物料品牌',
    materialEdit: '物料编辑',
    categoryEdit: '类型编辑'
  }
}
